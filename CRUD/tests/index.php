<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_erros', 1 );
error_reporting ( E_ALL );

define ( 'DS', DIRECTORY_SEPARATOR );

require_once __DIR__ . DS . '..' . DS . 'bootstrap.php';

use Classes\db\MyDatabase;
use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use Testes\tMyDatabase\tMyDatabase;
use Testes\tMyError\tMyError;

// echo 'OOK';
// printf ( "\n" );

/// MyError
//
// ( new tMyError (  ) )->myCustomErrorMessageTest (  );

/// MyDB
//
// ( new tMyDatabase (  ) )->testBasic (  );
// ( new tMyDatabase (  ) )->testQuery (  );
// ( new tMyDatabase (  ) )->testErrorConnection (  );
// ( new tMyDatabase (  ) )->testSeguence (  );
// ( new tMyDatabase (  ) )->testInsertDatabase (  );
// ( new tMyDatabase (  ) )->testUpdateDatabase (  );
// ( new tMyDatabase (  ) )->testDeleteDatabase (  );
// ( new tMyDatabase (  ) )->testSelectDatabase (  );
// ( new tMyDatabase (  ) )->testSeguence (  );

/// MyController
//
( new Controller (  ) )->seguenceStarter (  );
