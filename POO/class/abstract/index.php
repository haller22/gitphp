<?php

require_once ( "interface/index.php" );

abstract class Automovel implements Veiculo {

  private $modelo;
  private $motor;
  private $ano;

  public function getModelo (  ) {

    return $this->modelo;
  }

  public function setModelo ( $modelo ) {

    $this->modelo = $modelo;
  }

  public function getMotor (  ) {

    return $this->motor;
  }

  public function setMotor ( $motor ) {

    $this->motor = $motor;
  }

  public function getAno (  ): int {

    return $this->ano;
  }

  public function setAno ( $ano ) {

    $this->ano = $ano;
  }

  public function exibir (  ) {

    return array(
        'modelo' => $this->modelo,
        'motor' => $this->motor,
        'ano' => $this->ano
    );
  }

  public function __construct ( $modelo, $ano="", $motor="" ) {

    $this->modelo = $modelo;
    $this->motor = $motor;
    $this->ano = $ano;
  }

  public function acelerar ( $velocidade = "" ) {

    return "O " . $this->modelo . " acelerou: " . $velocidade;
  }

  public function frenar ( $velocidade = "" ) {
    return "O " . $this->modelo . " frenou: " . $velocidade;
  }

  public function trocarMarcha ( $marcha = "1" ) {

    return "O " . $this->modelo . " engatou: " . $marcha;
  }
};

abstract class Animal implements Acao {

  public function falar (  ) {

    return "Som";
  }

  public function mover (  ) {

    return "Anda";
  }
}
