<?php
namespace Classes\MyError;

interface ErrorInterface
{

  const E_UNDEFINED_ERROR = 100;
  const E_NOT_AUTH_USER = 203;
  const E_PARCIAL_INSERT_USER = 206;
  const E_TYPE_USER = 300;
  const E_INVALID_USER = 400;
  const E_BAD_CONNECTION_ERROR = 402;
  const E_NOT_FOLD_USER = 404;
  const E_NOT_PERMISS_USER = 405;
  const E_CONFLIT_USER = 409;
  const E_MEDIA_UNSUPORTED_USER = 415;
  const E_INTERNAL_ERROR = 500;
  const E_BAD_GATEWAY_ERROR = 502;
  // const E_ERROR_USER = CODE;

  public function formatedOut (  );
  public function formatedOutCase ( $code );
  public function showErrorJSON ( $jsonMessaError );
  public function showError ( $messaError );
  public function checkTypeError ( $error, $constError = MyError::E_UNDEFINED_ERROR );

}
