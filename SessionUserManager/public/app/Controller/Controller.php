<?php
namespace Controller;

require 'config.php';

use Classes\db\MyDatabase;
use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

class Controller
{

  private $mydb;
  const $tableName = 'test';

  private function instanceTradeDatabase (  )
  {

    $mydb = new MyDatabase (  );
  }

  public function delete (  )
  {

    $this->instanceTradeDatabase (  );

    $whereAgument = array (

      $_GET [ 'camp' ] => $_GET [ 'value' ]
    );

    $this->mydb->deleteIntoTable ( $tableName, $whereAgument );

  }

  public function insert (  )
  {

    $this->instanceTradeDatabase (  );

    $arrayValuesFields = array (

      'name' => $_GET [ 'name' ],
      'pass' => $_GET [ 'pass' ]
    );

    $this->mydb->insertIntoTable ( $tableName, $arrayValuesFields );

  }

  public function show (  )
  {

    $this->instanceTradeDatabase (  );

    $whereAgument = array (  );

    if ( !( is_null ( $_GET ) ) )
      $whereAgument = array (

        'argv' => array (

          $_GET [ 'argv' ]['camp'] => $_GET [ 'argv' ]['value']
        )
      );

    return json_encode ( $this->mydb->selectTable ( $tableName, $whereAgument['argv'] ) );
  }

  public function update (  )
  {

    $this->instanceTradeDatabase (  );

    $arrayValuesFields = array (

      'name' => $_GET [ 'argv' ][ 'name' ],
      'pass' => $_GET [ 'argv' ][ 'pass' ]
    );

    $whereAgument = array (  );

    if ( !( is_null ( $_GET [ 'where' ] ) ) )
      $whereAgument = array (

        'argv' => array (

          $_GET [ 'argv' ]['camp'] => $_GET [ 'argv' ]['value']
        )
      );

    $this->mydb->updateTable ( $tableName, $arrayValuesFields, $whereAgument );

  }

  public function __destruct (  )
  {

    $this->mydb = null;
  }
}
