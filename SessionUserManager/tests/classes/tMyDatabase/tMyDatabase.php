<?php
namespace Testes\tMyDatabase;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use Classes\db\MyDatabase;

class tMyDatabase
{

  public function testBasic (  )
  {

    $db = new MyDatabase (  );

    $db->conectDataBase (  );
  }

  public function testSelectDatabase (  )
  {

    $mydb = new MyDatabase (  );

    $tableName = 'test';

    echo json_encode ( $mydb->selectTable ( $tableName ) );
  }

  public function testSelectDatabaseWhereCondition (  )
  {

    $mydb = new MyDatabase (  );

    $tableName = 'test';

    $whereAgument = array (

      'name' => 'eliseu'
    );

    echo json_encode ( $mydb->selectTable ( $tableName, $whereAgument ) );

  }

  public function testUpdateDatabase (  )
  {

    $mydb = new MyDatabase (  );

    $tableName = 'test';

    $arrayValuesFields = array (

      'name' => 'eliseu',
      'pass' => '666689999'
    );

    $whereAgument = array (

      'name' => 'eliseu'
    );

    $mydb->updateTable ( $tableName, $arrayValuesFields, $whereAgument );

  }

  public function testDeleteDatabase (  )
  {

    $mydb = new MyDatabase (  );

    $tableName = 'test';

    $whereAgument = array (

      'name' => 'mario'
    );

    $mydb->deleteIntoTable ( $tableName, $whereAgument );

  }

  public function testInsertDatabase (  )
  {

    $mydb = new MyDatabase (  );

    $tableName = 'test';

    $arrayValuesFields = array (

      'name' => 'eliseu',
      'pass' => '666666666'
    );

    $mydb->insertIntoTable ( $tableName, $arrayValuesFields );
  }

  public function testQuery (  )
  {

    $result = 'falid';
    $mydb = new MyDatabase (  );
    printf ("1\n");
    $result = $mydb->queryTransaction ( 'SHOW DATABASES;' );
    printf ("2\n");
    echo json_encode ( $result );

  }

  public function testErrorConnection (  )
  {

    $mydb = new MyDatabase (  );

    echo "/////////////////////////////" ;//. "<br/>";
    printf ( "\n" );
    try {

      $mydb->conectDataBase (  );
      echo "conectado ao banco" . "<br/>";
      unset ( $mydb );
      echo "desconectando do banco";

    } catch ( \Exception $error ) {

      $error->checkTypeError ( $error );
    }
  }

  public function testSeguence (  )
  {

    $this->testSelectDatabase (  );
    $this->testInsertDatabase (  );
    echo ',';

    $this->testSelectDatabase (  );
    $this->testUpdateDatabase (  );
    echo ',';

    $this->testSelectDatabase (  );
    $this->testDeleteDatabase (  );
    echo ',';

    $this->testSelectDatabaseWhereCondition (  );
  }
}
