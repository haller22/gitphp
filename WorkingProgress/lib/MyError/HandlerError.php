<?php
namespace Classes\MyError;

use Classes\MyError\ErrorInterface;

abstract class HandlerError extends \Exception implements ErrorInterface
{

  protected function switchError ( $flag )
  {

    switch ( $flag ) {

      case self::E_NOT_AUTH_USER:
        return array (
          'E_NOT_AUTH_USER' => self::E_NOT_AUTH_USER
        );
        break;

      case self::E_PARCIAL_INSERT_USER:
        return array (
          'E_PARCIAL_INSERT_USER' => self::E_PARCIAL_INSERT_USER
        );
        break;

      case self::E_TYPE_USER:
        return array (
          'E_TYPE_USER' => self::E_TYPE_USER
        );
        break;

      case self::E_INVALID_USER:
        return array (
          'E_INVALID_USER' => self::E_INVALID_USER
        );
        break;

      case self::E_BAD_CONNECTION_ERROR:
        return array (
          'E_BAD_CONNECTION_ERROR' => self::E_BAD_CONNECTION_ERROR
        );
        break;

      case self::E_NOT_FOLD_USER:
        return array (
          'E_NOT_FOLD_USER' => self::E_NOT_FOLD_USER
        );
        break;

      case self::E_NOT_PERMISS_USER:
        return array (
          'E_NOT_PERMISS_USER' => self::E_NOT_PERMISS_USER
        );
        break;

      case self::E_CONFLIT_USER:
        return array (
          'E_CONFLIT_USER' => self::E_CONFLIT_USER
        );
        break;

      case self::E_MEDIA_UNSUPORTED_USER:
        return array (
          'E_MEDIA_UNSUPORTED_USER' => self::E_MEDIA_UNSUPORTED_USER
        );
        break;

      case self::E_TYPE_USER:
        return array (
          'E_TYPE_USER' => self::E_TYPE_USER
        );
        break;

      case self::E_INTERNAL_ERROR:
        return array (
          'E_INTERNAL_ERROR' => self::E_INTERNAL_ERROR
        );
        break;

      case self::E_BAD_GATEWAY_ERROR:
        return array (
          'E_BAD_GATEWAY_ERROR' => self::E_BAD_GATEWAY_ERROR
        );
        break;

      default:
        return array (
          'E_UNDEFINED_ERROR' => self::E_UNDEFINED_ERROR
        );
    }
  }

  public function checkTypeError ( $error, $constError = MyError::E_UNDEFINED_ERROR )
  {

    if ( $error instanceof MyCustomError )
      $error->showErrorJSON ( $error->formatedOut (  ) );
    else if ( $error instanceof MyError )
      $error->showErrorJSON ( $error->formatedOutCase ( $constError ) );
    else
      echo $error;
  }

  public function formatedOut (  )
  {

      return ( json_encode ( array (

        'code' => $this->code,
        'message' => $this->message,
        'line' => $this->line,
        'file' => $this->file
      ) ) );
  }

  public function formatedOutCase ( $code )
  {

      $this->message .= " FLAG : " . array_keys ( $this->switchError ( $code ) )[0];
      $this->code = $code;

      return ( json_encode ( array (

        'code' => $this->code,
        'message' => $this->message,
        'line' => $this->line,
        'file' => $this->file
      ) ) );
  }

  public function showErrorJSON ( $jsonMessaError )
  {

    echo $jsonMessaError;
  }

  public function showError ( $messaError )
  {

    echo json_decode ( $messaError );
  }

}
