<?php
namespace Classes\db;

interface DatabaseInterface
{

  const TYPE_DATABASE = 'mysql'; // mysql
  const ERROR_PATH_LOGS = '/var/log/server/error_logs.log';
  const HOST_DATABASE = 'mariadb'; // mariadb
  const USER_DATABASE_NAME = 'root'; // root
  const USER_DATABASE_PASSWD = 23; // 23
  const DATABASE_STD = 'test'; // test

}
