<?php
namespace Testes\tMyDatabase;

use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use Classes\db\MyDatabase;

class tMyDatabase
{

  private $mydb;
  private $tableName;

  private function instanceTradeDatabase (  )
  {

    $this->mydb = new MyDatabase (  );
    $this->tableName = 'test';
  }

  public function testBasic (  )
  {

    $this->mydb = new MyDatabase (  );

    $this->mydb->conectDataBase (  );
  }

  public function testSelectDatabase (  )
  {

    $this->mydb = new MyDatabase (  );

    echo json_encode ( $this->mydb->selectTable ( $this->tableName ) );
  }

  public function testSelectDatabaseWhereCondition (  )
  {

    $this->mydb = new MyDatabase (  );

    $whereAgument = array (

      'id' => '14',
      'name' => 'eliseu',
      'password' => '666689999'
    );

    echo json_encode ( $this->mydb->selectTable ( $this->tableName, $whereAgument ) );

  }

  public function testUpdateDatabase (  )
  {

    $this->mydb = new MyDatabase (  );

    $arrayValuesFields = array (

      'name' => 'eliseu',
      'password' => '666689999'
    );

    $whereAgument = array (

      'name' => 'eliseu'
    );

    $this->mydb->updateTable ( $this->tableName, $arrayValuesFields, $whereAgument );

  }

  public function testDeleteDatabase (  )
  {

    $this->mydb = new MyDatabase (  );

    $whereAgument = array (

      'name' => 'mario'
    );

    $this->mydb->deleteIntoTable ( $this->tableName, $whereAgument );

  }

  public function testInsertDatabase (  )
  {

    $this->mydb = new MyDatabase (  );

    $arrayValuesFields = array (

      'name' => 'eliseu',
      'password' => '666666666'
    );

    $this->mydb->insertIntoTable ( $this->tableName, $arrayValuesFields );
  }

  public function testQuery (  )
  {

    $result = 'falid';
    $this->mydb = new MyDatabase (  );
    printf ("1\n");
    $result = $this->mydb->queryTransaction ( 'SHOW DATABASES;' );
    printf ("2\n");
    echo json_encode ( $result );

  }

  public function testErrorConnection (  )
  {

    $this->mydb = new MyDatabase (  );

    echo "/////////////////////////////" ;//. "<br/>";
    printf ( "\n" );
    try {

      $this->mydb->conectDataBase (  );
      echo "conectado ao banco" . "<br/>";
      unset ( $this->mydb );
      echo "desconectando do banco";

    } catch ( \Exception $error ) {

      $error->checkTypeError ( $error );
    }
  }

  public function testSeguence (  )
  {

    $this->testSelectDatabase (  );
    echo ',';

    $this->testInsertDatabase (  );
    $this->testSelectDatabaseWhereCondition (  );
    $this->testInsertDatabase (  );
    echo ',';

    $this->testSelectDatabase (  );
    $this->testUpdateDatabase (  );
    echo ',';

    $this->testSelectDatabase (  );
    $this->testDeleteDatabase (  );
    echo ',';

    $this->testSelectDatabaseWhereCondition (  );
  }

  public function __construct (  )
  {

    $this->instanceTradeDatabase (  );
  }

  public function __destructIntoDatabase (  )
  {

    $this->mydb = null;
    $this->tableName = null;
  }
}
