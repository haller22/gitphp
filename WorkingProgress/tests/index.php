<?php
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_erros', 1 );
error_reporting ( E_ALL );


require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap.php';

use Classes\db\MyDatabase;
use Classes\MyError\MyCustomError;
use Classes\MyError\MyError;

use Testes\tMyDatabase\tMyDatabase;
use Testes\tMyError\tMyError;

use Testes\tMyController\tMyController;

use Testes\tMyManipulationDatabase\tMyManipulationDatabase;



// echo 'OOK';
// printf ( "\n" );

/// MyError
//
// ( new tMyError (  ) )->myCustomErrorMessageTest (  );

/// MyDB
//
// ( new tMyDatabase (  ) )->testBasic (  );
// ( new tMyDatabase (  ) )->testQuery (  );
// ( new tMyDatabase (  ) )->testErrorConnection (  );
// ( new tMyDatabase (  ) )->testSeguence (  );
// ( new tMyDatabase (  ) )->testInsertDatabase (  );
// ( new tMyDatabase (  ) )->testUpdateDatabase (  );
// ( new tMyDatabase (  ) )->testSelectDatabaseWhereCondition (  );
// ( new tMyDatabase (  ) )->testDeleteDatabase (  );
// ( new tMyDatabase (  ) )->testSelectDatabase (  );
// ( new tMyDatabase (  ) )->testSeguence (  );

/// MyController
//
// ( new tMyController (  ) )->seguenceStarter (  );


/// MyManipulationDatabase
//
( new tMyManipulationDatabase (  ) )->internalTestes (  );
